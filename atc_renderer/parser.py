#!/usr/bin/env python3
###################################################################################
# Parser for the test case renderer.
# This module defines the test case format and implement a parser for the format.
#
# Copyright (C) 2018
# Luis Araujo <luis.araujo@collabora.co.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import sys
import yaml

from atc_renderer.exceptions import ParserTypeError, ParserMissingFieldError

image_types = [ "any", "development", "minimal", "SDK", "target", "tiny-lxc" ]
image_arch = ['any', 'amd64', 'arm64', 'armhf']
tests_types = ['compatibility', 'functional', 'performance', 'sanity', 'system']
priorities = ['critical', 'high', 'low', 'medium']
execution_type = ['all', 'automated', 'manual']

# This structure defines the test case YAML file format.
test_case_format = {
    'metadata': (True, {
        'name':            (True,  ""),
        'format':          (False, ""),
        'image-type':      (True,  image_types),
        'image-arch':      (True,  image_arch),
        'type':            (True,  tests_types),
        'exec-type':       (True,  execution_type),
        'priority':        (True,  priorities),
        'maintainer':      (False, ""),
        'description':     (True,  ""),
        'resources':       (False, []),
        'pre-conditions':  (False, []),
        'expected':        (True,  []),
        'notes':           (False, []),
        'post-conditions': (False, [])
    }),
    'install': (False, {
        'deps': (True, [])
    }),
    'run': (True, {
        'steps': (True, [])
    }),
    'parse': (False, {})
}


def _parse_format(test_case, test_case_format):
    for tagf, valuestr in test_case_format.items():
        mandatory, valuef = valuestr
        value = test_case.get(tagf)
        if not value:
            if mandatory:
                raise ParserMissingFieldError("Mandatory field missing: " + tagf)
            # Test case doesn't have this non-mandatory tag, so just continue.
            continue

        if type(value) == str and type(valuef) == list:
            if tagf in ['image-type' , 'image-arch', 'type',
                        'exec-type', 'priority']:
                try:
                    valuef.index(value)
                except ValueError:
                    sys.stderr.write("Warning: value '{}' not found in '{}' for "\
                                     "field '{}'\n".format(value, valuef, tagf))
                continue

        if type(value) != type(valuef):
            raise ParserTypeError("Incorrect type for field: " + tagf)

        if type(value) == dict:
            _parse_format(value, valuef)

    return True

def parse_format(test_case):
    return _parse_format(test_case, test_case_format)


if '__main__' == __name__:
    testcase = sys.argv[1]
    with open(testcase) as test_case:
        test_case_data = yaml.safe_load(test_case)

    parse_format(test_case_data)
