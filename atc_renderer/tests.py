###################################################################################
# Unit tests for the test case parser and renderer.
#
# Copyright (C) 2018
# Luis Araujo <luis.araujo@collabora.co.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import yaml
import unittest
import os.path
import copy

from atc_renderer.parser import parse_format
from atc_renderer.renderer import parse_list, \
    generate_test_case_page, \
    PYTHON_PKGNAME
from atc_renderer.exceptions import ParserTypeError, ParserMissingFieldError

TESTS_FILES_PATH= PYTHON_PKGNAME + "/tests_files/"

def read_tc(filepath):
    with open(filepath) as test_case:
        return yaml.safe_load(test_case)

def read_file(filepath):
    with open(filepath) as test_case_page:
        return test_case_page.read()


class TestFieldTypes(unittest.TestCase):
    """
    Test detecting type errors for fields values in the parser.
    """

    def setUp(self):
        self.test_file = read_tc(os.path.join(TESTS_FILES_PATH, "test_file4.yaml"))

    def test_metadata_type(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'] = "a test"
        with self.assertRaises(ParserTypeError):
            parse_format(tc_file)

    def test_name_type(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata']['name'] = [ 'one', 'two' ]
        with self.assertRaises(ParserTypeError):
            parse_format(tc_file)

    def test_description_type(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata']['description'] = [ 'one', 'two' ]
        with self.assertRaises(ParserTypeError):
            parse_format(tc_file)

    def test_expected_type(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata']['expected'] = "a test"
        with self.assertRaises(ParserTypeError):
            parse_format(tc_file)

    def test_run_type(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['run'] = "a test"
        with self.assertRaises(ParserTypeError):
            parse_format(tc_file)

    def test_run_steps_type(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['run']['steps'] = "a test"
        with self.assertRaises(ParserTypeError):
            parse_format(tc_file)

        
class TestMissingFields(unittest.TestCase):
    """
    Test detecting missing mandatory fields in the test case file.
    """
    
    def setUp(self):
        self.test_file = read_tc(os.path.join(TESTS_FILES_PATH, "test_file1.yaml"))

    def test_missing_metadata_field(self):
        tc_file = self.test_file.copy()
        tc_file.pop('metadata')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_name_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('name')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_image_type_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('image-type')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_image_arch_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('image-arch')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_type_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('type')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_exec_type_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('exec-type')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_priority_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('priority')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_description_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('description')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_expected_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['metadata'].pop('expected')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_run_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file.pop('run')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)

    def test_missing_run_steps_field(self):
        tc_file = copy.deepcopy(self.test_file)
        tc_file['run'].pop('steps')
        with self.assertRaises(ParserMissingFieldError):
            parse_format(tc_file)


class TestParser(unittest.TestCase):
    """
    Test parsing some complete test case files.
    """
    
    def test_parsing_file1(self):
        tc_file = read_tc(os.path.join(TESTS_FILES_PATH, "test_file1.yaml"))
        self.assertTrue(parse_format(tc_file))
    
    def test_parsing_file2(self):
        tc_file = read_tc(os.path.join(TESTS_FILES_PATH, "test_file2.yaml"))
        self.assertTrue(parse_format(tc_file))

    def test_parsing_file3(self):
        tc_file = read_tc(os.path.join(TESTS_FILES_PATH, "test_file3.yaml"))
        self.assertTrue(parse_format(tc_file))

    def test_parsing_file4(self):
        tc_file = read_tc(os.path.join(TESTS_FILES_PATH, "test_file4.yaml"))
        self.assertTrue(parse_format(tc_file))

    def test_parsing_file5(self):
        tc_file = read_tc(os.path.join(TESTS_FILES_PATH, "test_file5.yaml"))
        self.assertTrue(parse_format(tc_file))


class TestListFormat(unittest.TestCase):
    """
    Test the output of the method that defines the format for the HTML lines
    (parse_list).
    """

    def test_comment_line(self):
        self.assertEqual(parse_list(["A comment"]),
                         [('A comment', '', '', '', '')])
    
    def test_command_line(self):
        self.assertEqual(parse_list(["$ ls -l"]), [('', '$ ls -l', '', '', '')])

    def test_output_line(self):
        self.assertEqual(parse_list([">output line"]),
                         [('', '', ['output line'], '', '')])

    def test_image_line(self):
        self.assertEqual(parse_list(["@image.png"]),
                         [('', '', '', 'image.png', '')])

    def test_web_link_line(self):
        self.assertEqual(parse_list(["~http://apertis.org"]),
                         [('', '', '', '', 'http://apertis.org')])

    # Use automated_run=True
    def test_automated_comment_line(self):
        self.assertEqual(parse_list(["# A comment"], automated_run=True),
                         [(' A comment', '', '', '', '')])

    def test_automated_command_line(self):
        self.assertEqual(parse_list(["ls -lt"], automated_run=True),
                         [('', '$ ls -lt', '', '', '')])


class TestRenderFile(unittest.TestCase):
    """
    Test rendering a test case YAML file and comparing the output to its expected
    HTML page.
    """

    def test_render_file1(self):
        tcfile = os.path.join(TESTS_FILES_PATH, "test_file1.yaml")
        page = read_file(os.path.join(TESTS_FILES_PATH, "test_file1.html"))
        self.assertEqual(generate_test_case_page(tcfile, None, return_out=True),
                         page)

    def test_render_file3(self):
        tcfile = os.path.join(TESTS_FILES_PATH, "test_file3.yaml")
        page = read_file(os.path.join(TESTS_FILES_PATH, "test_file3.html"))
        self.assertEqual(generate_test_case_page(tcfile, None, return_out=True),
                         page)

    def test_render_file4(self):
        tcfile = os.path.join(TESTS_FILES_PATH, "test_file4.yaml")
        page = read_file(os.path.join(TESTS_FILES_PATH, "test_file4.html"))
        self.assertEqual(generate_test_case_page(tcfile, None, return_out=True),
                         page)

    def test_render_file5(self):
        tcfile = os.path.join(TESTS_FILES_PATH, "test_file5.yaml")
        page = read_file(os.path.join(TESTS_FILES_PATH, "test_file5.html"))
        self.assertEqual(generate_test_case_page(tcfile, None, return_out=True),
                         page)
